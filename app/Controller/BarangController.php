<?php 
require_once('../app/Model/Barang.php');
require_once('../app/App/View.php');

class BarangController
{
    public static $model;

    public function __construct()
    {
        self::$model = new Barang();
    }

    public function showAll(): void
    {
        $data = self::$model->getAll();
        View::render("main",$data);
    }

    public function show($id): void
    {
        $data = self::$model->getById($id);
        var_dump($data);
    }

    public function add(): void
    {
        View::render("add",null);
    }

    public function store(): void
    {
        $name = $_POST["name"];
        $qty = $_POST["qty"];

        self::$model->addData($name, $qty);
        header("location:all");        
    }

    public function remove($id): void
    {
        self::$model->deleteData($id);
        header("location:../all");
    }

    public function edit($id): void
    {
        $data = self::$model->getById($id);
        View::render("edit", $data);
    }

    public function update($id): void
    {
        $name = $_POST["name"];
        $qty = $_POST["qty"];

        self::$model->updateData($id, $name, $qty);
        header("location:../all");
    }
}
?>