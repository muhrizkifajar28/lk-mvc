<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit barang</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="card w-50 mx-auto py-4 mt-5">
            <h2 class="py-2 px-4">Edit Barang</h2>
            <form action="../update/<?= $model['id'] ?>" method="post">
                <div class="py-2 px-4">
                    <label class="form-label" for="nameInput">Name</label>
                    <input class="form-control" type="text" name="name" id="nameInput" value="<?= $model['name'] ?>">
                </div>
                <div class="py-2 px-4">
                    <label class="form-label" for="qtyInput">Quantity</label>
                    <input class="form-control" type="number" name="qty" id="qtyInput" value="<?= $model['qty'] ?>">
                </div>
                <div class="py-2 px-4">
                    <input class="btn btn-success" type="submit" value="Update">
                </div>
            </form>
        </div>
    </div>
</body>
</html>