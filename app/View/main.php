<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD MVC</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
    <div class="container d-flex flex-column gap-3 mt-5">
        <h1 class="text-center">Inventory Barang</h1>
        <a href="add" class="btn btn-primary">Add Barang</a>
        <table class="table table-striped mt-4">
            <thead class="table table-dark">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>QTY</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($model as $row): ?>
                    <tr>
                        <td><?= $row['id'] ?></td>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['qty'] ?></td>
                        <td>
                            <a href="remove/<?= $row['id'] ?>" class="btn btn-danger">Hapus</a>
                            <a href="edit/<?= $row['id'] ?>" class="btn btn-warning">Edit</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</body>
</html>