<?php 
require_once("Model.php");
class Barang extends Model
{
    private $tablename = "barangs";
    private $id;
    private $name;
    private $qty;

    public function getAll()
    {
        $stmt = "SELECT * FROM {$this->tablename}";
        $query = $this->db
            ->query($stmt);
        $data = [];

        while(
            $result = $this->db
                ->fetch_array($query)
        ) {
            $data[] = $result;
        }

        return $data;
    }

    public function getById($id)
    {
        $stmt = "SELECT * FROM {$this->tablename} WHERE id = :id";
        $param = [
            'id' => $id
        ];

        $query = $this->db
            ->query($stmt, $param);
        
        $result = $this->db
            ->fetch_array($query);

        return $result;
    }

    public function addData($name, $qty)
    {
        $stmt = "INSERT INTO {$this->tablename} (name, qty) VALUES (:name, :qty)";
        $param = [
            'name'  => $name,
            'qty'   => $qty
        ];

        $query = $this->db
            ->query($stmt, $param);
    }

    public function deleteData($id)
    {
        $stmt = "DELETE FROM {$this->tablename} WHERE id = :id";
        $param = [
            'id' => $id
        ];

        $query = $this->db
            ->query($stmt, $param);
    }

    public function updateData($id, $name, $qty)
    {
        $stmt = "UPDATE {$this->tablename} SET name = :name, qty = :qty WHERE id = :id";
        $param = [
            'id'    => $id,
            'name'  => $name,
            'qty'   => $qty
        ];

        $query = $this->db
            ->query($stmt, $param);
    }
}
?>