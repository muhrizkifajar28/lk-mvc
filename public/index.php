<?php 
require_once("../app/App/Route.php");
require_once("../app/Controller/BarangController.php");

Route::add("GET", "/all", BarangController::class, "showAll");
Route::add("GET","/get/([0-9*])", BarangController::class, "show");
Route::add("GET","/add", BarangController::class, "add");
Route::add("POST","/store", BarangController::class, "store");
Route::add("GET","/remove/([0-9]*)", BarangController::class, "remove");
Route::add("GET","/edit/([0-9]*)", BarangController::class, "edit");
Route::add("POST","/update/([0-9]*)", BarangController::class, "update");

Route::run();
?>